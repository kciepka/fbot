(function () {
    'use strict';

    const login = require ('facebook-chat-api')

    var angularApp = angular.module("app", []);

    angularApp.controller("mainCtrl", function($scope){
      $scope.lastEvent = "";

      var getUserName = function(threadId, eventCallback){
        $scope.api.getThreadInfo(threadId, (err, info) => {
            if(err){
              $scope.lastEvent = $scope.getCurrentTime() +  err.error;
              $scope.$apply();
              return console.error(err);
            }
            else{
              var userNameArray = info.name.split(" ");
              var userName = userNameArray[0];
              eventCallback(userName);
            }
          });
      }

      var changeThreadColor = function(threadId, color){
        $scope.api.changeThreadColor(color, threadId, (err) => {
          if(err){
            $scope.lastEvent = $scope.getCurrentTime() +  err.error;
          }
          else{
            $scope.lastEvent = $scope.getCurrentTime() +  "Chat color changed";
         }
         $scope.$apply();
        });
      }

      var sendMessage = function(threadId, message){
        $scope.api.sendMessage(message, threadId, (err, messageInfo) => {
          if(err){
             $scope.lastEvent = $scope.getCurrentTime() + err.error;
           }
          else{
            $scope.lastEvent = $scope.getCurrentTime() +  "Response message sent correctly";
          }
          $scope.$apply();
        });
      }

      $scope.login = function(usr_email, usr_password, welcomeMsg, repeatMsg){
        login({email: usr_email, password: usr_password}, $scope.loginHandler);
      }

      $scope.logout = function(){
        $scope.api.logout((err) => {
          if(err){
            $scope.lastEvent = $scope.getCurrentTime() +  err.error;
            return console.error(err);
          }
          $scope.loggedIn = false;
          $scope.lastEvent = $scope.getCurrentTime() + "Logout successful";
          $scope.$apply();
        });
      }

      $scope.logIn = function(){
        $scope.login($scope.email, $scope.password, $scope.welcomeMsg, $scope.repeatMsg);
      }

      $scope.logOut = function(){
        $scope.logout();
      }

      $scope.loginHandler = (err, api) => {
          $scope.api = api;
          if(err){
            $scope.loggedIn = false;
            $scope.lastEvent = $scope.getCurrentTime() +  err.error;
            $scope.$apply();
            return console.error(err);
          }
          $scope.loggedIn = true;
          $scope.lastEvent = $scope.getCurrentTime() + "Login successful"
          $scope.$apply();
          api.setOptions({selfListen: true});
          api.listen($scope.receivedMessageHandler);
      }

      $scope.receivedMessageHandler = (err, message) => {
          $scope.lastEvent = $scope.getCurrentTime() + "Message has been received";
          var thread = message.threadID;
          var isGroup = message.isGroup;
          var sender = message.senderID;
          var isItMe = ($scope.api.getCurrentUserID() == sender);
          if(isItMe){
            if(message.body == "bot stop"){
              $scope.pause = true;
              changeThreadColor(thread, "#0084ff");
              $scope.lastEvent = $scope.getCurrentTime() + "Bot has been stopped";
              $scope.$apply();
              return;
            }
            else if(message.body == "bot start"){
              $scope.threadIds = [];
              $scope.pause = false;
              $scope.lastEvent = $scope.getCurrentTime() + "Bot has been started";
              $scope.$apply();
              return;
            }
          }
          else if(!isGroup){
            if($scope.pause) return;

            if(global.threadIds.indexOf(thread) != -1){
              changeThreadColor(thread, "#ff0000");
              var responseMsg = repeatMsg;
              if(message.body != null && message.body != "") sendMessage(thread, responseMsg);
            }
            else{
              global.threadIds.push(thread);
              getUserName(thread, (userName) => {
                changeThreadColor(thread, "#ffff00");
                var responseMsg = userName + "," + welcomeMsg;
                sendMessage(thread, responseMsg);
              });
            }
          }
      }

      $scope.getCurrentTime = function(){
        var date = new Date();
        return date + ": ";
      }
    });
})();
